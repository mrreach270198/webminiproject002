import "./App.css";
import MyNav from "./components/MyNav";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container } from "react-bootstrap";
import GetInfo from "./components/GetInfo";
import DisplayInfo from "./components/DisplayInfo";
import { useState } from "react";
import MyTable from "./components/MyTable";
import MyCard from "./components/MyCard";

function App() {
  let [person, setPerson] = useState([]);
  const [update, setUpdate] = useState({})
  const [display, setDisplay] = useState("Table");

  const changeDisplay = (type) => {
    setDisplay(type);
  };

  let getData = (data) => {
    setPerson([...person, data]);
  };

  let onDelete = (item) => {
    const afterDel = person.filter((name) => {
      return name.uid !== item.uid;
    });
    setPerson(afterDel);
  };

  let onUpdate = (item) =>{
    setUpdate(item)
  }

  return (
    <div>
      <MyNav />
      <Container className="my-5">
        <GetInfo 
        getData={getData} data={update}/>
        <DisplayInfo
          className="my-3"
          changeDisplay={changeDisplay}
          display={display}
        />
        {display === "Table" ? (
          <MyTable data={person} onDelete={onDelete} onUpdate={onUpdate} />
        ) : (
          <MyCard data={person} onDelete={onDelete} onUpdate={onUpdate} />
        )}
      </Container>
    </div>
  );
}
export default App;
