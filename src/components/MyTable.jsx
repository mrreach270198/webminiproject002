import React, { useState } from "react";
import { Table, Button, Modal, Row, Col, Container } from "react-bootstrap";
import { BsEye, BsPencilSquare } from "react-icons/bs";
import { RiDeleteBin6Line } from "react-icons/ri";

function MyTable({ data, onDelete, onUpdate }) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <div>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Email</th>
            <th>Job</th>
            <th>Create At</th>
            <th>Update at</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item, i) => (
            <tr key={i}>
              <td>{item.uid}</td>
              <td>{item.username}</td>
              <td>{item.gender}</td>
              <td>{item.email}</td>
              <td>
                {item.job.map((job, index) => (
                  <ul key={index}>
                    <li>{job}</li>
                  </ul>
                ))}
              </td>
              <td>{item.createAt}</td>
              <td>00:00</td>
              <td>
                <Button
                  type="button"
                  variant="primary"
                  size="sm"
                  onClick={handleShow}
                >
                  <BsEye />
                  View
                </Button>
                <Button 
                type="button" 
                className="mx-2" 
                variant="info" 
                size="sm"
                onClick={() => onUpdate(item)}>
                  <BsPencilSquare /> Update
                </Button>
                <Button
                  type="button"
                  variant="danger"
                  size="sm"
                  onClick={() => onDelete(item)}
                > <RiDeleteBin6Line/>
                  Delete
                </Button>
              </td>
              {
                <Modal show={show} centered onHide={handleClose}>
                  <Modal.Header>
                    <Modal.Title>Information</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <Container>
                      <Row>
                        <Col md="6">
                          <h4>{item.username}</h4>
                          <h5>{item.email}</h5>
                          <h5>{item.gender}</h5>
                        </Col>
                        <Col md="6">
                          <h5>Job</h5>
                          {item.job.map((job, index) => (
                            <ul key={index}>
                              <li>{job}</li>
                            </ul>
                          ))}
                        </Col>
                      </Row>
                    </Container>
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="primary" onClick={handleClose}>
                      Close
                    </Button>
                  </Modal.Footer>
                </Modal>
              }
            </tr>
          ))}
        </tbody>
      </Table>
      {
        data.length===0 ? <div className="nodata my-4 "></div> : ''
      }
    </div>
  );
}

export default MyTable;
