import React from "react";
import { useState } from "react";
import { Button, ButtonGroup } from "react-bootstrap";

export default function DisplayInfo({ changeDisplay, display }) {
  const [displayType, setDisplayType] = useState(["Table", "Card"]);
  return (
    <div>
      <h2>Display Data As :</h2>

      <ButtonGroup aria-label="type" className="my-4 pl-1">
        {displayType.map((type, idx) => (
          <Button
            key={idx}
            variant="secondary"
            active={type === display}
            onClick={() => changeDisplay(type)}
          >
            {type}
          </Button>
        ))}
      </ButtonGroup>
    </div>
  );
}
