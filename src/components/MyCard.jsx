import React, { useState } from "react";
import {
  Card,
  DropdownButton,
  Dropdown,
  Row,
  Col,
  Container,
  Modal,
  Button,
} from "react-bootstrap";

function MyCard({ data, onDelete }) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <div>
      <Row>
        {data.map((item, i) => (
          <Col md="3" key={i}>
            <Card>
              <Card.Body>
              <Modal.Header>
                <DropdownButton
                  variant="success"
                  className="dropdown m-auto"
                  id="dropdown-basic-button"
                  title="Action">
                  <Dropdown.Item onClick={handleShow}>View</Dropdown.Item>
                  <Dropdown.Item>Update</Dropdown.Item>
                  <Dropdown.Item onClick={() => onDelete(item)}>
                    Delete
                  </Dropdown.Item>
                </DropdownButton>
                </Modal.Header>
                <Card.Title className="my-2">ID : {item.uid}</Card.Title>
                <Card.Title >Name : {item.username}</Card.Title>
                <Card.Title>Email : {item.email}</Card.Title>
                <Card.Title>Gender : {item.gender}</Card.Title>
                <Card.Text>
                  <h5>Job</h5>
                  {item.job.map((job, index) => (
                    <ul key={index}>
                      <li>{job}</li>
                    </ul>
                  ))}
                </Card.Text>
                <Modal.Footer >
                <Card.Text className="m-auto">
                  {item.createAt}
                </Card.Text> 
                </Modal.Footer>
              </Card.Body>
            </Card>

            <Modal show={show} centered onHide={handleClose}>
              <Modal.Header>
                <Modal.Title>Information</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Container>
                  <Row>
                    <Col md="6">
                      <h4>{item.username}</h4>
                      <h5>{item.email}</h5>
                      <h5>{item.gender}</h5>
                    </Col>
                    <Col md="6">
                      <h5>Job</h5>
                      {item.job.map((job, index) => (
                        <ul key={index}>
                          <li>{job}</li>
                        </ul>
                      ))}
                    </Col>
                  </Row>
                </Container>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="primary" onClick={handleClose}>
                  Close
                </Button>
              </Modal.Footer>
            </Modal>
          </Col>
        ))}
      </Row>
      {
        data.length===0 ? <div className="nodata my-4 "></div> : ''
      }
    </div>
  );
}
export default MyCard;
