import React, { useEffect } from "react";
import { Col, Row, Form, Button } from "react-bootstrap";
import { useState } from "react";
import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';

export default function GetInfo({ getData, data }) {

  var uid = uuidv4().slice(0, 5);
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [gender, setGender] = useState("Male");
  const [job, setJob] = useState(["Student"]);
  const [createAt , setCreateAt] = useState(moment().startOf('second').fromNow())
  const [jobCheckBox, setJobCheckBox] = useState([
    "Student",
    "Teacher",
    "Developer",
  ]);
  const [genderRadio, setGenderRadio] = useState(["Male", "Female"]);

  const handleSubmit = (e) => {
    getData({uid, username, email, gender, job, createAt })
        setUsername("")
        setEmail("")
        data = ""
        // setGender("Male")
        // setJob(
        //   "Student",
        //   "Teacher",
        //   "Developer",
        // )
  }

  return (
    <div>
      <h1>Personal Info</h1>
      <Row>
        <Col md="8">
          <Form>
            <Form.Group className="mb-3" controlId="formBasicUsername">
              <Form.Label>Username</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter username"
                value={username === ""? data.username : username}
                onChange={(e) => setUsername(e.target.value)}
                
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email === ""? data.email : email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>
            <Button variant="dark" type="button" className="myBtn">
              Cancel
            </Button>
            <Button
              className="myBtn"
              variant="primary"
              type="reset"
              onClick={() => handleSubmit()}>
              {/* {data.uid === ""? "Submit" : "Save"} */}
              Submit 
            </Button>
          </Form>
        </Col>

        <Col md="4">
          <h3>Gender</h3>
          <Form.Group className="fg mb-3" controlId="formBasicRadio">
            {genderRadio.map((item, i) => (
              <Form.Check
                key={i}
                type="radio"
                label={item}
                name="gender"
                value={item}
                onChange={(e) => setGender(e.target.value)}
                defaultChecked={item === "Male" ? true : false}
                className="fc"
              />
            ))}
          </Form.Group>

          <h3>Job</h3>
          <Form.Group className="fg mb-3" controlId="formBasicCheckbox">
            {jobCheckBox.map((item, i) => (
              <Form.Check
                key={i}
                type="checkbox"
                label={item}
                value={item}
                defaultChecked={item === "Student" ? true : false}
                onChange={(e) => setJob([...job, e.target.value])}
                className="fc"
              />
            ))}
          </Form.Group>
        </Col>
      </Row>
    </div>
  );
}
